import React, {useState} from "react";
import {TextField, Button, Box} from '@material-ui/core';

export default function Login ({onSubmit}) {

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleSubmit = (event) => {
        event.preventDefault();
        onSubmit({username, password})
    }

    return (
        <form onSubmit={handleSubmit} autoComplete='off' data-testid="loginForm">
            <Box m={3} component={'label'}>
                username
                <TextField
                    label="username"
                    data-testid="loginForm-username"
                    value={username}
                    onChange={(event) => setUsername(event.target.value)}
                />
            </Box>
            <Box m={3} component={'label'}>
                password
                <TextField
                    label="password"
                    data-testid="loginForm-password"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
            </Box>
            <Box m={3}>
                <Button type='submit' variant="contained" color="primary">
                    Log in
                </Button>
            </Box>
        </form>
    )
}
